# TodoLuby

Consiste em uma aplicação de *Todo List* desenvolvida com *React.js*

## Requisitos

- [x] A estilização (simples) deve ser com https://styled-components.com.

- [x] Um campo de texto para digitar a tarefa e um botão adicionar.

- [x] Implemente a validação no campo, para que a tarefa não seja incluída na lista sem conteúdo. A tarefa deve ter no mínimo 5 caracteres.

- [x] (Não obrigatório) Se possuir conhecimentos, utilize https://github.com/jaredpalmer/formik para criar o form.

- [x] (Não obrigatório) Se possuir conhecimentos, utilize https://github.com/jquense/yup para validação.

- [x] Gravar as tarefas em uma localstorage. 

- [x] (Não obrigatório) Se possuir conhecimentos em redux, utilize reducers e actions para a tarefa de add e list.

- [x] (Não obrigatório) Criar botão para ordenar a lista.

- [ ] (Não obrigatório) Se possível utilize o next js para renderizar a página.

## Execução

1) Após clonar o reposotório e acessar a pasta, execute o seguinte comando para instalar as dependências:
```
npm i
``` 
2) E então, execute o seguinte comando para rodar o projeto em [http://localhost:3000](http://localhost:3000)
```
npm start
```
