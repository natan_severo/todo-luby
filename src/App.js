import React from 'react';

import TaskForm from './components/task-form/task-form.component';
import TaskList from './components/task-list/task-list.component';

const App = () => (
  <>
    <h1>TodoLuby</h1>
    <TaskForm />
    <TaskList />
  </>
);

export default App;