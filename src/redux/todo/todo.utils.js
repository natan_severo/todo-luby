export const toggleATaskState = (todoList, todoIndex) => {
    const todoListCopy = JSON.parse(JSON.stringify(todoList));
    todoListCopy[todoIndex].done = !todoListCopy[todoIndex].done;
    
    return todoListCopy;
}

export const sortByUncompleted = todoList => {
    const allNotDoneTasks = todoList.filter(task => !task.done);
    const allDoneTasks = todoList.filter(task => task.done);

    return [...allNotDoneTasks, ...allDoneTasks];
}

export const sortByCreation = todoList => {
    const todoListCopy = JSON.parse(JSON.stringify(todoList));
    
    return todoListCopy.sort((firstObj, secondObj) => firstObj.createdAt - secondObj.createdAt); 
}