import TodoTypes from './todo.types';

export const addTodo = taskName => {
    const task = {
        taskName,
        createdAt: Date.now(),
        done: false
    }

    return {
        type: TodoTypes.ADD_TODO,
        payload: task
    }
}

export const toggleTodo = index => ({
    type: TodoTypes.TOGGLE_TODO,
    payload: index
});

export const sortTodoByCreation = () => ({
    type: TodoTypes.SORT_TODO_BY_CREATION,
});

export const sortTodoByUncompleted = () => ({
    type: TodoTypes.SORT_TODO_BY_UNCOMPLETED,
});