import TodoTypes from './todo.types';

import { toggleATaskState, sortByCreation, sortByUncompleted } from './todo.utils';

const INITIAL_STATE = {
    list: []
}

const todoReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TodoTypes.ADD_TODO:
            return {
                ...state,
                list: [...state.list, action.payload]
            }

        case TodoTypes.TOGGLE_TODO:
            return {
                ...state,
                list: toggleATaskState(state.list, action.payload)
            }

        case TodoTypes.SORT_TODO_BY_CREATION:
            return {
                ...state,
                list: sortByCreation(state.list)
            }

        case TodoTypes.SORT_TODO_BY_UNCOMPLETED:
            return {
                ...state,
                list: sortByUncompleted(state.list)
            }

        default:
            return state;
    }
}

export default todoReducer;