import React from 'react';
import { connect } from 'react-redux';

import { sortTodoByUncompleted, sortTodoByCreation } from '../../redux/todo/todo.actions';

import { SortActionsContainer, SortButtonContainer } from './sort-actions.styles';

const SortActions = ({ sortTodoByUncompleted, sortTodoByCreation }) => (
    <SortActionsContainer>
        <SortButtonContainer onClick={() => sortTodoByUncompleted()} name='Sort by Uncompleted' />
        <SortButtonContainer onClick={() => sortTodoByCreation()} name='Sort by Created' />
    </SortActionsContainer>
)

const mapDispatchToProps = dispatch => ({
    sortTodoByUncompleted: () => dispatch(sortTodoByUncompleted()),
    sortTodoByCreation: () => dispatch(sortTodoByCreation())
})

export default connect(null, mapDispatchToProps)(SortActions);