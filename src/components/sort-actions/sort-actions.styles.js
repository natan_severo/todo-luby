import styled from 'styled-components';
import Button from '../button/button.component';

export const SortActionsContainer = styled.div`
    margin-bottom: 10px;
`

export const SortButtonContainer = styled(Button)`
    width: 160px;
    margin-right: 10px;
`