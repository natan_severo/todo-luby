import * as Yup from 'yup';

const TaskFormSchema = Yup.object().shape({
    taskName: Yup.string()
        .min(5, 'A tarefa deve possuir no mínimo 5 caracteres!')
        .required('Nome da tarefa é obrigatória!')
});

export default TaskFormSchema;