import styled from 'styled-components';
import { Field } from 'formik';

export const TaskFormContainer = styled.div`
    margin-bottom: 16px;
`;

export const FieldContainer = styled(Field)`
    padding: 8px 4px 8px 4px;
    font-size: 14px;
    margin-right: 6px;
    border-radius: 2px;
    border-color: transparent;
    width: 300px;
`

export const ErrorMessageContainer = styled.div`
    color: #d30000;
`