import React from 'react';
import { Formik, Form } from 'formik';
import { connect } from 'react-redux';

import TaskFormSchema from './task-form.schema';

import Button from '../button/button.component';

import { TaskFormContainer, FieldContainer, ErrorMessageContainer } from './task-form.styles';

import { addTodo } from '../../redux/todo/todo.actions';

const TaskForm = props => {
    const handleSubmit = (values, { resetForm }) => {
        props.addTodo(values.taskName);
        resetForm();
    }

    return (
        <TaskFormContainer>
            <Formik
                initialValues={{
                    taskName: ''
                }}
                validationSchema={TaskFormSchema}
                onSubmit={handleSubmit}
            >
                {
                    ({ errors, touched }) => (
                        <Form>
                            <FieldContainer name='taskName' placeholder='Type a task...' />

                            <Button type='submit' name='Add' />

                            {errors.taskName && touched.taskName && <ErrorMessageContainer>{errors.taskName}</ErrorMessageContainer>}
                        </Form>
                    )
                }
            </Formik>
        </TaskFormContainer>
    );
}

const mapDispatchToProps = dispatch => ({
    addTodo: taskName => dispatch(addTodo(taskName)),
});

export default connect(null, mapDispatchToProps)(TaskForm);