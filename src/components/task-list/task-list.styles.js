import styled, { css } from 'styled-components';

export const TaskListContainer = styled.ul`
    text-align: left;
    width: 100%;
    list-style-type: none;
    margin: 0;
    padding: 0;
`;

const getListItemStyle = props => {
    return props.done ? doneTask : null;
}

const doneTask = css`
    text-decoration: line-through;
    opacity: 0.5;
`;

export const ListItemContainer = styled.li`
    font-size: 16px;
    padding: 4px 0 4px 0;
    border: 1px solid #c1c1c1;
    width: 405px;
    border-radius: 2px;
    margin-bottom: 2px;

    ${getListItemStyle}
`;

export const CheckboxContainer = styled.input`
    width: 20px;
    height: 16px;
    margin-right: 8px;
`;
