import React from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import { selectTodoList } from '../../redux/todo/todo.selectors';

import { toggleTodo } from '../../redux/todo/todo.actions';

import { TaskListContainer, ListItemContainer, CheckboxContainer } from './task-list.styles';
import SortActions from '../sort-actions/sort-actions.component';

const TaskList = ({ todoList, toggleTodo }) => (
    <TaskListContainer>
        <SortActions />
        
        {
            todoList.length > 0
            &&
            todoList.map((todo, index) => (
                <ListItemContainer key={index} done={todo.done}>
                    <CheckboxContainer type='checkbox' checked={todo.done} onChange={() => toggleTodo(index)} />

                    {todo.taskName}
                </ListItemContainer>
            ))
        }
    </TaskListContainer>
)

const mapStateToProps = createStructuredSelector({
    todoList: selectTodoList
});

const mapDispatchToProps = dispatch => ({
    toggleTodo: todoIndex => dispatch(toggleTodo(todoIndex)),
})

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);