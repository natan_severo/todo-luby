import styled from 'styled-components';

export const ButtonContainer = styled.button`
  background-color: #536DFE;
  color: white;
  border-color: #536DFE;
  width: 90px;
  font-size: 14px; 
  border-radius: 2px;
  padding: 8px 0 8px 0;
  font-weight: bold;
`;