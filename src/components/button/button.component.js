import React from 'react';

import { ButtonContainer } from './button.styles';

const Button = ({ name, ...otherProps }) => (
    <ButtonContainer {...otherProps}>
        {name}
    </ButtonContainer>
);

export default Button;